package com.nikoschantzopoulos.engine;

/**
 * A custom Exception class used in 
 * the program to notify users when something goes wrong.
 * @author Nikolaos Chantzopoulos
 * version 0.1
 * @since 11-18-2017
 */
public class CustomException extends RuntimeException {
	
	private static final long serialVersionUID = 11182017L;

	
	@Override
	public String getMessage() {
		
		return "Triangle program failed to determine triangle type. "
				+ "Make sure you have non empty non zero sides";
	}
	
	
	

}
